
conjuntoVacio = set()

conjunto = {'RJH-224', 'QBC-443', 'RJH-224'}

print(conjunto)

placas = ['RJH-224', 'QBC-443', 'RJH-224']

conversion = set(placas)
print(conversion)

frase = 'Hola mundo'
frase_conjunto = set(frase)
print(frase_conjunto)

frase_lista = list(frase_conjunto)
print(frase_lista)

print('---------------------------')

bd_placas_1 = {'RJH-224', 'QBC-443', 'QJH-224'}
bd_placas_2 = {'AJH-224', 'ABC-443', 'RJH-224'}

placas_en_comun = bd_placas_1.intersection(bd_placas_2)
print('Placas en común-> ', placas_en_comun)


diferencia = bd_placas_1.difference(bd_placas_2)
print(diferencia)

bd_placas_1.add('ABC-432')

print('----------------DE DICCIONARIO A CONJUNTOS-------------')
diccionario_placas = {
    'placa_1': 'RJH-224',
    'placa_2': 'ABC-443',
    'placa_3': 'QJH-224'
}

conjunto_placas = set( diccionario_placas.values() )
print(conjunto_placas)